import fitbit
from selenium import webdriver
import gather_keys_oauth2 as oauth
import requests
import base64

driver = webdriver.Chrome()
url = "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=######&redirect_uri=http%3A%2F%2F127.0.0.1%3A8080%2F&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight"

driver.get(url)
consumer_key = "######"
consumer_secret = "###################"
server = oauth.OAuth2Server
server = oauth.OAuth2Server(consumer_key, consumer_secret)
server.browser_authorize()
access_token = str(server.fitbit.client.session.token['access_token'])
refresh_token = str(server.fitbit.client.session.token['refresh_token'])
auth2_client = fitbit.Fitbit(consumer_key, consumer_secret, oauth2=True, access_token=access_token, refresh_token=refresh_token) 